package pl.codementors.lambdas;

import java.util.*;


public class App {
    public static void main(String[] args) {
        List<String> words = new ArrayList<>();
        words.add("foo");
        words.add("bars");
        words.add("");
        words.add("bazuka");
        words.add("ann");
        words.add("biana");

        System.out.println("task1:");
        task1(words);
        System.out.println("\ntask2:");
        task2(words);
        System.out.println("\ntask3:");
        task3(words);
        System.out.println("\ntask4:");
        task4(words);
        System.out.println("\ntask5:");
        task5(words);
        System.out.println("\ntask6:");
        task6(words);
        System.out.println("\ntask7:");
        task7(words);
        System.out.println("\ntask8:");
        task8(words);
        System.out.println("\ntask9:");
        task9(words);
        System.out.println("\ntask10:");
        task10(words);
        System.out.println("\ntask11:");
        task11(words);
        System.out.println("\ntask12:");
        task12(words);
        System.out.println("\ntask13:");
        task13(words);

        SetList(words);
        NewList(words);
    }

    public static void task1(List<String> words) {
        //Wypisanie wszystkich słów
        words.forEach(word -> System.out.println(word));
    }

    public static void task2(List<String> words) {
        //Wypisanie liczby znakow kazdego elementu listy
        words.stream().map(s -> s.length()).forEach(System.out::println);
    }

    public static void task3(List<String> words) {
        //Wypisanie sumy liczby znaków
        System.out.println(words.stream().mapToInt(word -> word.length()).sum());
    }

    public static void task4(List<String> words) {
        //Wypisanie najmniejszej liczby znakow
        System.out.println(words.stream().mapToInt(word -> word.length()).min().orElse(-1));
    }

    public static void task5(List<String> words) {
        // Wypisanie najwiekszej liczby znakow
        System.out.println(words.stream().mapToInt(word -> word.length()).max().orElse(-1));
    }

    public static void task6(List<String> words) {
        //Wypisac wszystkie elementy listy zapisane duzymi literami
        words.forEach(element -> System.out.println(element.toUpperCase()));
    }

    public static void task7(List<String> words) {
        //Wypisanie wszystkich niepustych elementow z listy
        words.stream().filter(s -> s.length() > 0).forEach(s -> System.out.println(s));
    }

    public static void task8(List<String> words) {
        //  Wypisanie elementow zaczynajacych sie na "a"
        words.stream().filter(word -> (word.startsWith("a")))
                .forEach(word -> System.out.println(word));
    }

    public static void task9(List<String> words) {
        // Wypisanie wszystkich elementów konczacych się na "a"
        words.stream().filter(word -> (word.endsWith("a")))
                .forEach(word -> System.out.println(word));
    }

    public static void task10(List<String> words) {
        // Wypisanie wszystkich elementow listy posortowanych alfabetycznie
        words.stream().sorted().forEach(System.out::println);
    }

    public static void task11(List<String> words) {
        // Wypisanie wszystkich elementów posortowanych po liczbie znaków
        words.stream().sorted((word1, word2) -> word1.length() - word2.length())
                .forEach(word -> System.out.println(word));
    }

    public static void task12(List<String> words) {
        //Wypisanie liczby znaków dla każdego elementu posortowane malejąco.
        words.stream()
                .sorted(Comparator.comparing(String::length).reversed())
                .forEach(System.out::println);
    }

    public static void task13(List<String> words) {
        //Wypisanie liczby wszystkich znaków dla każdego elementu posortowanych rosnąco z pominięciem pustych elementów
        words.stream()
                .filter(item -> !item.isEmpty())
                .sorted(Comparator.comparing(String::length))
                .forEach(item -> System.out.println(item.length()));

    }

    public static void SetList (List<String> list){
        Set<String> set = new TreeSet<>();
        list.stream()
                .filter(item -> !item.isEmpty())
                .forEach(item -> set.add(item));
        set.forEach(System.out::println);
    }

    public static void NewList (List<String> list){
        List<String> sortedList = new ArrayList<>();
        list.sort(String::compareToIgnoreCase);
        list.stream()
                .filter(item -> !item.isEmpty())
                .forEach(item -> sortedList.add(item));
        sortedList.forEach(System.out::println);
    }

}
